package com.ppb.openday.livedata.domain;

public class ResponseError {
    public static final String UNEXPECTED = "Unexpected error";

    private String message;

    public ResponseError() {}

    public ResponseError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
