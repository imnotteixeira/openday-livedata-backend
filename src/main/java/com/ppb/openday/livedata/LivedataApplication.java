package com.ppb.openday.livedata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude={CassandraAutoConfiguration.class})
public class LivedataApplication {

	/**
	 * The main of this Spring Boot application. Implements lots of magic behind the scenes to make our lifes
	 * easier... Sometimes.
	 * https://spring.io/projects/spring-boot
	 */
	public static void main(String[] args) {
		SpringApplication.run(LivedataApplication.class, args);
	}
}
