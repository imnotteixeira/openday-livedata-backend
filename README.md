### Openday backend

Open the project as a Maven project (File > Open > (Select the pom.xml) > Open as project )

Compile ```mvn clean install``` (or go through the IDE in Maven Projects)

Run ```java -jar target/livedata-0.0.1-SNAPSHOT.jar```