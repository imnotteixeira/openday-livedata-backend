package com.ppb.openday.livedata.controllers;

import com.ppb.openday.livedata.domain.FootballLivescores;
import com.ppb.openday.livedata.domain.ResponseError;
import com.ppb.openday.livedata.persistence.LivedataPersistence;
import com.ppb.openday.livedata.persistence.RepositoryException;
import com.ppb.openday.livedata.persistence.SqliteLivedataPersistence;
import com.ppb.openday.livedata.persistence.converters.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Implements the REST API and two endpoints for managing livescores (see method livescores and method addLivedata.
 */
@RestController
public class LivescoresController {

    private static final Logger logger = LoggerFactory.getLogger(LivescoresController.class);

    // controls the persistence of the livescores
    @Autowired
    private LivedataPersistence livedataPersistence;

    // Converts a FootballLivescores object into a String ready to be saved in the database
    @Autowired
    private Converter<FootballLivescores, String> converter;

    // Converts a String retrieved from the database into a FootballLivescores instance ready to be served.
    @Autowired
    private Converter<String, FootballLivescores> footballLivescoresConverter;


    /**
     * This method is the entrypoint for retrieving livescores of a certain eventId. Call it in a browser with
     * http://localhost:8080/livedata?eventId=<any_string>
     * @param eventId the event id to retrieve
     * @param response the final response object that is returned to the browser (you won't need to touch this).
     * @return a FootballLivescores instance containing the relevant data for the eventId.
     */
    @GetMapping("/livedata")
    public FootballLivescores livescores(@RequestParam(value="eventId") String eventId, HttpServletResponse response) {
        logger.info("Received /livedata request for eventId = {}", eventId);
        if (response != null) {
            response.setHeader("Access-Control-Allow-Origin", "*");
        }
        FootballLivescores fls = null;
        // TODO: use the Autowired interfaces of this class to retrieve livedata and convert it to a FootballLivescores instance.
        try {
            String liveScore = livedataPersistence.getEventContext(eventId);
            fls = converter.convert(liveScore);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        return fls;
    }

    /**
     * This method is the entrypoint for updating livescores for a certain eventId.
     * This endpoint is better invoked with software like Postman, because this is a POST request (see HTTP fundamentals
     * for more context).
     * @param livescores the instance of FootballLivescores to save in the database.
     * @return A response.
     */
    @PostMapping("/addLivedata")
    public ResponseEntity addLivedata(@RequestBody FootballLivescores livescores) {
        logger.info("Received /addLivedata request.");
        System.out.println("Livescore: " + livescores.getEventId() + " " + livescores.getHome().getName());

        // TODO use the Autowired interfaces to convert and save the incoming livescores into the database.
        String s = footballLivescoresConverter.convert(livescores);
        try {
            livedataPersistence.saveEventContext(livescores.getEventId(),s);
        } catch (RepositoryException e) {
            e.printStackTrace();
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
