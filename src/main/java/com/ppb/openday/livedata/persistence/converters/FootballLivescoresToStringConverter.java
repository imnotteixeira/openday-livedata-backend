package com.ppb.openday.livedata.persistence.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ppb.openday.livedata.domain.FootballLivescores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class FootballLivescoresToStringConverter implements Converter<String, FootballLivescores> {

    private static final Logger logger = LoggerFactory.getLogger(FootballLivescoresToStringConverter.class);

    /**
     * Converts a FootballLivescores instance into a JSON string (google JSON for more info)
     * This converter uses the jackson JSON implementation.
     */
    @Override
    public String convert(FootballLivescores input) {
        if (input == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(input);
        } catch (JsonProcessingException e) {
            logger.error("Error while converting football livescores to json", e);
            return null;
        }
    }
}
