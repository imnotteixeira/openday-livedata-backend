package com.ppb.openday.livedata.persistence.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ppb.openday.livedata.domain.FootballLivescores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class StringToFootballLivescoresConverter implements Converter<FootballLivescores, String> {

    private static final Logger logger = LoggerFactory.getLogger(StringToFootballLivescoresConverter.class);

    /**
     * Converts a JSON string into a FootballLivescores instance.
     * @param input
     * @return
     */
    @Override
    public FootballLivescores convert(String input) {
        if (input == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(input, FootballLivescores.class);
        } catch (Exception e) {
            logger.error("Error while unmarshalling football livescores to domain", e);
            return null;
        }
    }
}
